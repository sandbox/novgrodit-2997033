# Commerce Klik and Pay Connect

Klik and Pay - Online Payment solution. Bringning you online secured payment since 2000. Worldwide payment for 
all online businesses. Secure, easy and reliable. We help your online business grow. This module integrates 
their Connect APIs for online payments and inventory into Drupal Commerce 2.x.

## Getting started

Review [Klik and Pay: Back Office]  to get your account id and sandbox access configured.

## Klik and Pay Payment Form

This module provides a Drupal Commerce payment gateway that integrates with 
[Klik and Pay Form]. This allows you to collect payment using your
account online.

See [Testing] to test the integration.
Test cards
Only the following cards can be used for successful transactions in TEST mode.

| Card       | Number           |
| ---------- | ---------------- |
| Visa       | 012888888881881  |
| Mastercard | 5555555555554444 |

The expected CVV is 111 and you can enter any future date for the expiry date .


In order to test the [Testing] you must enable the test mode and add the administrator's email.

[Klik and Pay Form]: 
http://knpmodules.com/doc/en/#direct-payment-and-example
[Klik and Pay: Back Office]: 
https://www.klikandpay.com/marchands/index.cgi
[Testing]: 
http://knpmodules.com/doc/en/#test-production-mode
