<?php

namespace Drupal\commerce_klikandpay\PluginForm\KlikAndPay;

use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

class PaymentOffsiteForm extends BasePaymentOffsiteForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    global $base_url;

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $payment_configuration = $payment_gateway_plugin->getConfiguration();
    $order = $payment->getOrder();
    $redirect_url = Url::fromUri($payment_configuration['url_redirect'])->toString();
    $billing = $order->getBillingProfile();
    /** @var \Drupal\address\Plugin\Field\FieldType\AddressItem $address */
    $address = $billing->get('address')->first();
    $address_line = $address->getAddressLine1()
      . (!empty($address->getAddressLine2()) ? ' ' . $address->getAddressLine2() : '');
    $amount = \Drupal::getContainer()
      ->get('commerce_price.rounder')
      ->round($payment->getAmount())
      ->getNumber();
    $language =  $this->getL(\Drupal::languageManager()->getCurrentLanguage()->getId());
    $data = [
      'ID' => $payment_configuration['account_id'],
      'NOM' => $address->getGivenName(),
      'PRENOM' => $address->getFamilyName(),
      'ADRESSE' => $address_line,
      'CODEPOSTAL' => $address->getPostalCode(),
      'VILLE' => $address->getLocality(),
      'PAYS' => $address->getCountryCode(),
      'EMAIL' => $order->getEmail(),
      'MONTANT' => $amount,
      'L' => $language,
      'DETAIL' => t('Payment of the order @order_id. The amount of @amount. 
         The site @base_url', ['@order_id' => $order->id(), '@amount' => $amount,
         '@base_url' => $base_url]),
      'RETOURVOK' => str_replace($base_url, '', $form['#return_url']),
      'RETOURVHS' => str_replace($base_url, '', $form['#cancel_url']),
    ];
    if (!empty($address->getOrganization())) $data['SOCIETE'] = $address->getOrganization();

    /** @todo: test mode */
    if ($payment_configuration['mode'] == 'test' && $payment_configuration['test_email']) {
      $data['EMAIL'] = $payment_configuration['test_email'];
    }

    return $this->buildRedirectForm($form, $form_state, $redirect_url, $data, 'post');
  }

  /**
   * Get the language of the payment page and of the emails sent to customers.
   *
   * @param string $lancode
   *
   * @return string
   */
  private function getL($lancode) {
    $language = [
      'fr' => 'FR',
      'en' => 'EN',
      'de' => 'DE',
      'es' => 'ES',
      'it' => 'IT',
      'du' => 'DU',
    ];
    return array_key_exists($lancode, $language) ? $language[$lancode] : 'EN';
  }

}
