<?php

namespace Drupal\commerce_klikandpay\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides the Klik&Pay payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "klikandpay",
 *   label = "Klik&Pay",
 *   display_label = "Klik&Pay",
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_klikandpay\PluginForm\KlikAndPay\PaymentOffsiteForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "amex", "dinersclub", "discover", "jcb", "mastercard", "visa",
 *   },
 * )
 */
class KlikAndPay extends OffsitePaymentGatewayBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
        'account_id' => '',
        'url_redirect' => 'https://www.klikandpay.com/paiement/order1.pl',
        'test_email' => '',
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    global $base_url;
    $url = Url::fromUri('https://www.klikandpay.com/marchands/index.cgi');

    $form['help'] = [
      '#type' => 'item',
      '#title' => t('Setting Klik&Pay back office'),
      '#markup' => $this->t('Add URL @base_url to fields "URL refused/cancelled transaction"
        and "URL transaction accepted" in your 
        <a href="@link">Klik&Pay back office</a> under “Account Set-up” 
        then under the sub-menu “Set-up”.', ['@link' => $url->toString(),
        '@base_url' =>  $base_url]),
    ];
    $form['account_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Account ID'),
      '#default_value' => $this->configuration['account_id'],
      '#description' => $this->t('You can find your merchant ID in your 
        <a href="@link">Klik&Pay back office</a> under “Account Administration” 
        then under the sub-menu “Account Information”.', ['@link' => $url->toString()]),
      '#required' => TRUE,
    ];
    $form['url_redirect'] = [
      '#type' => 'url',
      '#title' => $this->t('URL redirect'),
      '#default_value' => $this->configuration['url_redirect'],
      '#required' => TRUE,
    ];
    $form['test_email'] = [
      '#type' => 'email',
      '#title' => $this->t("Administrator's email"),
      '#description' => $this->t("Administrator's email testing indicated in your Klik&Pay back office."),
      '#default_value' => $this->configuration['test_email'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['account_id'] = $values['account_id'];
      $this->configuration['url_redirect'] = $values['url_redirect'];
      $this->configuration['test_email'] = $values['test_email'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    /** @var \Drupal\commerce_payment\Entity\Payment $payment */
    $payment = $payment_storage->create([
      'state' => 'authorization',
      'amount' => $order->getTotalPrice(),
      'payment_gateway' => $this->entityId,
      'order_id' => $order->id(),
      'test' => $this->getMode() == 'test',
      'remote_state' => 'Success',
    ]);
    $payment->save();
    $messenger = \Drupal::messenger();
    $messenger->addMessage($this->t('Payment was successfully processed'));
  }

  /**
   * {@inheritdoc}
   */
  public function onCancel(OrderInterface $order, Request $request) {
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    /** @var \Drupal\commerce_payment\Entity\Payment $payment */
    $payment = $payment_storage->create([
      'state' => 'authorization_voided',
      'amount' => $order->getTotalPrice(),
      'payment_gateway' => $this->entityId,
      'order_id' => $order->id(),
      'test' => $this->getMode() == 'test',
      'remote_state' => 'Failure',
    ]);
    $payment->save();
    $messenger = \Drupal::messenger();
    $messenger->addError($this->t('Payment failed'));
  }
}
